#!/usr/bin/env python
# coding: utf-8

#making a note of the initial starting time
import datetime
a = datetime.datetime.now().replace(microsecond=0)
print(a)

#importing all necessary libraries
import matplotlib.pyplot as plt
import numpy as np
import os
import cv2
import PIL
import zipfile
import numpy as np
import tensorflow as tf
from keras.preprocessing.image import load_img
from tensorflow.keras.optimizers import Adam
from keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing import image
#tf.keras.utils.load_img
from tensorflow.keras.preprocessing.image import ImageDataGenerator
#get_ipython().run_line_magic('load_ext', 'tensorboard')
import datetime
#ignore warnings if any while executing cells
import warnings
warnings.filterwarnings('ignore')


#loading the image from path
img = image.load_img("/Users/karthibalasundaram/Downloads/Deep_learning_datas/Birds-Dataset/Train/FLAME TANAGER/021.jpg")


#diaplaying the image shape
cv2.imread("/Users/karthibalasundaram/Downloads/Deep_learning_datas/Birds-Dataset/Train/FLAME TANAGER/027.jpg").shape


#checking whether the path is read
plt.imshow(img)


#rescaling the train ansd validation images to (0-1) using ImageDataGenerator
train = ImageDataGenerator(rescale =1/255)
validation = ImageDataGenerator(rescale =1/255)


#defining train and validation datasets
train_dataset = train.flow_from_directory('/Users/karthibalasundaram/Downloads/Deep_learning_datas/Birds-Dataset/Train',
                                          target_size=(200,200))
validation_dataset = validation.flow_from_directory('/Users/karthibalasundaram/Downloads/Deep_learning_datas/Birds-Dataset/Validation',
                                          target_size=(200,200))


#displaing the classes and their indices, classes here means the bird's name
train_dataset.class_indices


#printing the train dataset classes
train_dataset.classes


#printing the validation dataset classes
validation_dataset.class_indices


#defining the sequential model
model = tf.keras.Sequential([tf.keras.layers.Conv2D(16,(3,3),activation = 'relu',input_shape =(200,200,3)),
                             tf.keras.layers.MaxPool2D(2,2),
                             tf.keras.layers.Conv2D(32,(3,3),activation = 'relu'),
                             tf.keras.layers.MaxPool2D(2,2),
                             tf.keras.layers.Conv2D(64,(3,3),activation = 'relu'),
                             tf.keras.layers.MaxPool2D(2,2),
                             tf.keras.layers.Conv2D(128,(3,3),activation = 'relu'),
                             tf.keras.layers.MaxPool2D(2,2),
                             
                             tf.keras.layers.Flatten(),
                             tf.keras.layers.Dropout(0.4),
                             tf.keras.layers.Dense(512,activation='relu'),
                             
                             tf.keras.layers.Dense(10,activation='softmax')
                             
])
#path for saving logs with timestamp value
path = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=path,histogram_freq=1)


#displaying the summary of the model
model.summary()

#compiling the model using optimizer Adam
model.compile(optimizer=Adam(learning_rate=0.001),
 loss='categorical_crossentropy',
 metrics = ['acc'])


tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=path,histogram_freq=1)

#training the model with 20 epochs
model_fit = model.fit(train_dataset, epochs=20, validation_data= validation_dataset, callbacks=[tensorboard_callback])


#defining a function to predict the trained model
def test():
    for i in range(10):
        if pred [0][i] ==1:
            break
            print("Model predicted class is",i)
    if i ==0:
        print("Model predicted bird name is FLAME TANAGER")
    elif i ==1:
        print("Model predicted bird name is GOULDIAN FINCH")
    elif i ==2:
        print("Model predicted bird name is INDIGO BUNTING")
    elif i ==3:
        print("Model predicted bird name is LILAC ROLLER")
    elif i ==4:
        print("Model predicted bird name is MALACHITE KINGFISHER")
    elif i == 5:
        print("Model predicted bird name is NICOBAR PIGEON")
    elif i==6:
        print("Model predicted bird name is PAINTED BUNTIG")
    elif i ==7:
        print("Model predicted bird name is PEACOCK")
    elif i ==8:
        print("Model predicted bird name is RAINBOW LORIKEET")
    else:
        print("Model predicted bird name is TAIWAN MAGPIE")
    print("**************************************************************")

#predicting the model and the results are displayed below
pred_dir_path = '/Users/karthibalasundaram/Downloads/Deep_learning_datas/Birds-Dataset/Test/'
print("Testing data local path :",pred_dir_path)
for j in os.listdir(pred_dir_path):
    if j.startswith('.'):
        continue
    print("Name of the testing bird",j)
    img = image.load_img(pred_dir_path+ '//'+ j, target_size=(200,200))
    plt.imshow(img)
    plt.show()
    x = image.img_to_array(img)
    x = np.expand_dims(x,axis =0)
    images = np.vstack([x])
    pred = model.predict(images, batch_size=10)
    print("Predicted value array",pred)
    test()

#calling the tensor board
get_ipython().run_line_magic('tensorboard', '--logdir logs/fit')

#saving the model for easier future access
model.save('model.h5')

#calculating the total time for the model execution
b = datetime.datetime.now().replace(microsecond=0)
print(b)
print("Total time for execution :",b-a)



